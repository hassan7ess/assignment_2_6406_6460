package com.company;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "restaurant")
@XmlAccessorType(XmlAccessType.FIELD)

public class Restaurant {

    @XmlElement(name = "users")
    private Users users;

    @XmlElement(name = "dishes")
    private Dishes dishes;

    @XmlElement(name = "tables")
    private Tables tables;

    public Users getUsers() {
        return users;
    }
    public void setUsers(Users users) {
        this.users = users;
    }
    public Dishes getDishes() {
        return dishes;
    }
    public void setDishes(Dishes dishes) {
        this.dishes = dishes;
    }
    public Tables getTables() {
        return tables;
    }
    public void setTables(Tables tables) {
        this.tables = tables;
    }
}
