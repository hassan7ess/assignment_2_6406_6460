package com.company;

import com.company.System.XMLInOut;
import com.company.dashboards.CookDashboard;

import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

public class Cook extends User {

    private CookDashboard cookDashboard;
    private Reservations reservations;
    private Restaurant restaurant;

    public void setCookDashboard(CookDashboard cookDashboard) {
        this.cookDashboard = cookDashboard;
    }

    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Cook (User user){
        super.setName(user.getName());
        super.setRole(user.getRole());
        super.setUsername(user.getUsername());
        super.setPassword(user.getPassword());
    }

    public void pickUpOrder(){
        DefaultTableModel cookTableModel = (DefaultTableModel) cookDashboard.cookTable.getModel();
        String orderDone= new String("");
        if(cookTableModel.getRowCount()>0)
            orderDone = (String) cookTableModel.getValueAt(0,0);
        int rowCount =cookTableModel.getRowCount();
        for (int i = 0; i <rowCount; i++) {
            cookTableModel.removeRow(0);
        }
        for (int i = 0; i < reservations.getReservations().size();i++){
            for (int j = 0;j <reservations.getReservations().get(i).getResOrder().size();j++){
                if(reservations.getReservations().get(i).getResOrder().get(j).getName().equals(orderDone) &&reservations.getReservations().get(i).getResOrder().get(j).getAmount()>0)
                    reservations.getReservations().get(i).getResOrder().get(j).setAmount(reservations.getReservations().get(i).getResOrder().get(j).getAmount()-1);
                if (reservations.getReservations().get(i).getResOrder().get(j).getAmount()>0)
                    cookTableModel.addRow(new Object[]{reservations.getReservations().get(i).getResOrder().get(j).getName(),reservations.getReservations().get(i).getResOrder().get(j).getAmount(),reservations.getReservations().get(i).getResTable().getNumber()});
            }
        }
        try {
            XMLInOut.reservationsMarshaller(reservations);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
