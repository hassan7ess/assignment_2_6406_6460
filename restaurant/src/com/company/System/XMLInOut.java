package com.company.System;

import com.company.Reservation;
import com.company.Reservations;
import com.company.Restaurant;

import javax.xml.bind.*;
import java.io.File;
import java.util.ArrayList;

public class XMLInOut  {

    public static String getPath() {

        String path = System.getProperty("user.dir");
        return path;

    }
    public static void Marshaller(Restaurant restaurant) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Restaurant.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();


        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(restaurant, new File(getPath() + "/restaurant.xml"));
    }
    public static Restaurant UnMarshaller() throws JAXBException{
        JAXBContext jaxbContext = JAXBContext.newInstance(Restaurant.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        Restaurant restaurant = (Restaurant) jaxbUnmarshaller.unmarshal( new File(getPath() + "/restaurant.xml") );

        return restaurant;
    }

    public static void reservationsMarshaller(Reservations reservations)throws JAXBException{
        JAXBContext jaxbContext = JAXBContext.newInstance(Reservations.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();


        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(reservations, new File(getPath() + "/reservations.xml"));
    }
    public static Reservations reservationsUnMarshaller() throws JAXBException{
        JAXBContext jaxbContext = JAXBContext.newInstance(Reservations.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        Reservations reservations = (Reservations) jaxbUnmarshaller.unmarshal( new File(getPath() + "/reservations.xml") );

        return reservations;
    }
}
