package com.company.System;

import com.company.*;

import javax.swing.*;

public class Authenticator {
    public static User login(Restaurant restaurant , String username,String password )
    {
        for(User temp : restaurant.getUsers().getUsersArray()) {
            if(temp.getUsername().equals(username) && temp.getPassword().equals(password)) {
                return temp;
            }
        }
        JOptionPane.showMessageDialog(null,"Incorrect Username or Password");
        return null;
    }
}
