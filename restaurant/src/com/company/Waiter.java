package com.company;

import com.company.System.XMLInOut;
import com.company.dashboards.WaiterDashboard;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

public class Waiter extends User {

    private WaiterDashboard waiterDashboard;
    private Reservations reservations;
    private Restaurant restaurant;

    public void setWaiterDashboard(WaiterDashboard waiterDashboard) {
        this.waiterDashboard = waiterDashboard;
    }
    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Waiter(User user){
        super.setName(user.getName());
        super.setRole(user.getRole());
        super.setUsername(user.getUsername());
        super.setPassword(user.getPassword());
    }

    public void deliverOrder(){
        DefaultTableModel cookTableModel = (DefaultTableModel) waiterDashboard.waiterTable.getModel();
        String orderCustName=new String();
        boolean orderMore = true;
        if(cookTableModel.getRowCount()>0)
            orderCustName = (String) cookTableModel.getValueAt(0,0);
        int rowCount =cookTableModel.getRowCount();
        for (int i = 0; i <rowCount; i++) {
            cookTableModel.removeRow(0);
        }
        for (int i = 0; i < reservations.getReservations().size();i++){

            int orderCount = reservations.getReservations().get(i).getResOrder().size();
            for (int j = 0;j < orderCount ; j++) {
                orderMore = false;
                if (reservations.getReservations().get(i).getName().equals(orderCustName) && reservations.getReservations().get(i).getResOrder().get(j).getAmount() != 0){
                    orderMore = true;
                    cookTableModel.addRow(new Object[]{reservations.getReservations().get(i).getName(), reservations.getReservations().get(i).getResTable().getNumber()});
                    JOptionPane.showMessageDialog(null,"current order not cooked yet");
                    break;
                }
            }
            if (orderMore == false){
                restaurant.getTables().getTables().get(reservations.getReservations().get(i).getResTable().getNumber()-1).setBooked(false);
                reservations.getReservations().remove(i);

            }
        }
        try {
            XMLInOut.reservationsMarshaller(reservations);
            XMLInOut.Marshaller(restaurant);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
