package com.company;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name = "tables")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tables {
    @XmlElement(name = "table")
    private ArrayList<Table> tables ;

    public ArrayList<Table> getTables() {
        return tables;
    }
    public void setTables(ArrayList<Table> tables) {
        this.tables = tables;
    }
}
