package com.company;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name = "reservation")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reservation {

    @XmlElement(name = "reservationID")
    private int reservationID;

    @XmlElement(name = "table")
    private Table resTable;

    @XmlElement(name = "price")
    private double resCost;

    @XmlElement(name = "Order")
    private ArrayList<Order> resOrder = new ArrayList<>();

    @XmlElement(name = "Customer")
    private String Name;
    //private Restaurant restaurant;

   /* public Reservation(Restaurant restaurant) {
        this.resTable = null;
        this.restaurant = restaurant;
        for (int i = 0;i < this.restaurant.getDishes().getDishes().size();i++) {
            resOrder.add(new Order(this.restaurant.getDishes().getDishes().get(i).getName(),this.restaurant.getDishes().getDishes().get(i).getPrice(),this.restaurant.getDishes().getDishes().get(i).getType(),0));
        }

    }*/
   private int Table;

    public int getTable() {
        return Table;
    }

    public void setTable(int table) {
        this.Table = table;
    }

    public Reservation(Restaurant restaurant) {
       this.resTable = null;
       for (int i = 0;i < restaurant.getDishes().getDishes().size();i++) {
           resOrder.add(new Order(restaurant.getDishes().getDishes().get(i).getName(),restaurant.getDishes().getDishes().get(i).getPrice(),restaurant.getDishes().getDishes().get(i).getType(),0));

       }
   }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setResOrder(ArrayList<Order> resOrder) {
        this.resOrder = resOrder;
    }

    public ArrayList<Order> getResOrder() {
        return resOrder;
    }

    public double getResCost() {
        return resCost;
    }

    public void setResCost(double resCost) {
        this.resCost = resCost;
    }

    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public Table getResTable() {
        return resTable;
    }

    public void setResTable(Table resTable) {
        this.resTable = resTable;
    }

    public Reservation(){}
}
