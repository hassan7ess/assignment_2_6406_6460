package com.company;



public class Order extends Dish{

    private int amount;

    public Order(String name, double price, String type, int amount) {
        super(name, price, type);
        this.amount = amount;
    }

    @Override
    public double getPrice() {
        switch (this.getType()){
            case "main_course":
            return 1.15*super.getPrice();
            case "appetizer":
            return 1.1*super.getPrice();
            case "desert":
            return 1.2*super.getPrice();
            default:return getPrice();
        }
    }

    public Order(){}

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
