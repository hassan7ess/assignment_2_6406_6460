package com.company;

import com.company.System.XMLInOut;
import com.company.mainScreen.DefaultScreen;

import javax.xml.bind.JAXBException;

public class Main {

    public static void main(String[] args) throws JAXBException {
        Restaurant restaurant = XMLInOut.UnMarshaller();
        DefaultScreen StartUpScreen = new DefaultScreen();
        StartUpScreen.setRestaurant(restaurant);
        StartUpScreen.setVisible(true);
    }

}
