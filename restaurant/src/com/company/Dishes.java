package com.company;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name = "dishes")
@XmlAccessorType (XmlAccessType.FIELD)
public class Dishes {

    @XmlElement(name = "dish")
    private ArrayList<Dish> dishes ;

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public void setDishes( ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }
}
