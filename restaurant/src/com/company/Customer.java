package com.company;

import com.company.System.XMLInOut;
import com.company.dashboards.CustomerScreen;
import com.company.dashboards.TableSelection;
import com.company.dashboards.WaiterDashboard;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.event.WindowEvent;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class Customer extends User {

    private CustomerScreen customerScreen;
    private Reservation reservation;
    private Restaurant restaurant;


    public void setCustomerScreen(CustomerScreen customerScreen) {
        this.customerScreen = customerScreen;
    }
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Customer(User user) {
        super.setName(user.getName());
        super.setRole(user.getRole());
        super.setUsername(user.getUsername());
        super.setPassword(user.getPassword());
    }

    public void addDishes(){
        if(customerScreen.custMenu.getSelectedValue() == null)
            JOptionPane.showMessageDialog(null,"Choose a dish form the menu.");
        else {

            String order[] = new String[restaurant.getDishes().getDishes().size()];
            int amountOfDish;

            String[] currentItem = customerScreen.custMenu.getSelectedValue().split("    ");
            Double price = Double.parseDouble(currentItem[currentItem.length - 1]);
            String[] newItem = Arrays.copyOf(currentItem, currentItem.length - 1);
            String compItem = String.join(" ", newItem);

            for (int i = 0; i < reservation.getResOrder().size(); i++) {

                if (reservation.getResOrder().get(i).getName().equals(compItem)) {
                    reservation.getResOrder().get(i).setAmount(reservation.getResOrder().get(i).getAmount() + 1);
                    //System.out.println("Beiber Fever");
                }
                amountOfDish = reservation.getResOrder().get(i).getAmount();
                if (reservation.getResOrder().get(i).getAmount() != 0) {
                    DecimalFormat DF = new DecimalFormat("0.00");
                    order[i] = (amountOfDish + "x   " + reservation.getResOrder().get(i).getName() + "     " + DF.format(reservation.getResOrder().get(i).getPrice() * amountOfDish) + "L.E\t" + '\n');
                    customerScreen.totalPrice = reservation.getResOrder().stream().map(N -> N.getPrice() * N.getAmount()).reduce(0D, (a, b) -> a + b);
                }
            }
            customerScreen.Invoice.removeAll();

            customerScreen.Invoice.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {
                    return order.length;
                }

                public String getElementAt(int i) {
                    return order[i];
                }
            });
            DecimalFormat bf = new DecimalFormat("0.00");
            customerScreen.tPrice.setText(bf.format(customerScreen.totalPrice) + "  L.E");
        }

    }

    public void placeOrder() throws JAXBException {
        if(reservation.getResOrder().isEmpty())
            JOptionPane.showMessageDialog(null,"No Items Selected");
        else if(reservation.getResTable() == null)
            JOptionPane.showMessageDialog(null,"No Table Selected");
        else{
            reservation.setResCost(customerScreen.totalPrice);
            reservation.setName(super.getName());
            Reservations res = new Reservations();
            try{
                res = XMLInOut.reservationsUnMarshaller();
                int lastID = res.getReservations().stream().map(n->n.getReservationID()).max(Integer::compare).get();
                reservation.setReservationID(lastID+1);
                res.getReservations().add(this.reservation);
                reservation.getResTable().setBooked(true);
                XMLInOut.Marshaller(restaurant);
                XMLInOut.reservationsMarshaller(res);
                JOptionPane.showMessageDialog(null,"order placed successfully");
                customerScreen.dispose();
            }catch(java.lang.IllegalArgumentException exc ){
                reservation.setReservationID(1);
                res.getReservations().add(reservation);
                reservation.getResTable().setBooked(true);
                XMLInOut.Marshaller(restaurant);
                XMLInOut.reservationsMarshaller(res);
                JOptionPane.showMessageDialog(null,"order placed successfully");
                customerScreen.dispose();
            }catch(java.util.NoSuchElementException excs){
                reservation.setReservationID(1);
                res.getReservations().add(reservation);
                reservation.getResTable().setBooked(true);
                XMLInOut.Marshaller(restaurant);
                XMLInOut.reservationsMarshaller(res);
                JOptionPane.showMessageDialog(null,"order placed successfully");
                customerScreen.dispose();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
    }

    public void selectTable(){
        TableSelection tableSel = new TableSelection();
        tableSel.setRestaurant(restaurant);
        tableSel.setcScreen(customerScreen);
        tableSel.setPrevTable(reservation.getResTable());
        tableSel.setVisible(true);
    }

    public void cancelOrder() throws JAXBException {
        if (reservation.getResTable() != null) {
            for (int i = 0; i < restaurant.getTables().getTables().size(); i++) {
                if (restaurant.getTables().getTables().get(i) == reservation.getResTable())
                    restaurant.getTables().getTables().get(i).setBooked(false);
            }
            XMLInOut.Marshaller(restaurant);
        }
        reservation = null;
        customerScreen.dispatchEvent(new WindowEvent(customerScreen, WindowEvent.WINDOW_CLOSING));
    }

    public void removeDish(){

        if(customerScreen.custMenu.getSelectedValue() == null)
            JOptionPane.showMessageDialog(null,"Choose a dish form the menu.");
        else {

            String order[] = new String[restaurant.getDishes().getDishes().size()];
            int amountOfDish;

            int itemToBeReducedIndex = customerScreen.Invoice.getSelectedIndex();

            reservation.getResOrder().get(itemToBeReducedIndex).setAmount(reservation.getResOrder().get(itemToBeReducedIndex).getAmount() - 1);
            customerScreen.totalPrice -= reservation.getResOrder().get(itemToBeReducedIndex).getPrice();
            for (int i = 0; i < reservation.getResOrder().size(); i++) {

                amountOfDish = reservation.getResOrder().get(i).getAmount();
                if (reservation.getResOrder().get(i).getAmount() != 0) {
                    DecimalFormat DF = new DecimalFormat("0.00");
                    order[i] = (amountOfDish + "x   " + reservation.getResOrder().get(i).getName() + "     " + DF.format(reservation.getResOrder().get(i).getPrice() * amountOfDish) + "L.E\t" + '\n');
                }
            }
            customerScreen.Invoice.removeAll();

            customerScreen.Invoice.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {
                    return order.length;
                }

                public String getElementAt(int i) {
                    return order[i];
                }
            });
            DecimalFormat bf = new DecimalFormat("0.00");
            customerScreen.tPrice.setText(bf.format(customerScreen.totalPrice) + "  L.E");

        }

    }
}
