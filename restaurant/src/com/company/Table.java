package com.company;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.FIELD)
public class Table {
    private int number;
    private int number_of_seats;
    private boolean smoking;
    private boolean booked;

    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public int getNumberOfSeats() {
        return number_of_seats;
    }
    public void setNumberOfSeats(int numberOfSeats) {
        this.number_of_seats = numberOfSeats;
    }
    public boolean isSmoking() {
        return smoking;
    }
    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }
    public boolean isBooked() {
        return booked;
    }
    public void setBooked(boolean booked) {
        this.booked = booked;
    }
}
