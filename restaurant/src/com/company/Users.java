package com.company;

import javax.xml.bind.annotation.*;
import java.util.*;

@XmlRootElement(name = "users")
@XmlAccessorType (XmlAccessType.FIELD)
public class Users {

    @XmlElement(name = "user")
    private ArrayList<User> users ;

    public ArrayList<User> getUsersArray() {
        return users;
    }

    public void setUsersArray( ArrayList<User> users) {
        this.users = users;
    }
}
