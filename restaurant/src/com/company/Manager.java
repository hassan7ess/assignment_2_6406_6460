package com.company;

import com.company.System.XMLInOut;
import com.company.dashboards.ManagerDashboard;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;
import java.util.ArrayList;



public class Manager extends User {

    private Restaurant restaurant;
    private Reservations reservations;
    private ManagerDashboard managerDashboard;

    public void setManagerDashboard(ManagerDashboard managerDashboard) {
        this.managerDashboard = managerDashboard;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setReservations(Reservations reservations) {
        this.reservations = reservations;
    }

    public Manager(User user) {
        super.setName(user.getName());
        super.setRole(user.getRole());
        super.setUsername(user.getUsername());
        super.setPassword(user.getPassword());
    }

    public void employCustomer(){
        DefaultComboBoxModel newJobModel = (DefaultComboBoxModel) managerDashboard.newJob.getModel();
        if(managerDashboard.custList.getSelectedValue() == null)
            JOptionPane.showMessageDialog(null,"Choose a Customer to Employ.");
        else{
            for (int i=0;i<restaurant.getUsers().getUsersArray().size();i++) {
                if (restaurant.getUsers().getUsersArray().get(i).getName().equals(managerDashboard.custList.getSelectedValue())) {
                    restaurant.getUsers().getUsersArray().get(i).setRole((String) newJobModel.getSelectedItem());
                    try {
                        XMLInOut.Marshaller(restaurant);
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        managerDashboard.custListAncestorAdded(null);
        managerDashboard.employeeListAncestorAdded(null);
    }

    public void fireEmployee(){
        DefaultComboBoxModel newJobModel = (DefaultComboBoxModel) managerDashboard.newJob.getModel();
        if(managerDashboard.employeeList.getSelectedValue() == null)
            JOptionPane.showMessageDialog(null,"Choose a Customer to Employ.");
        else{
            String[] nameEmp = managerDashboard.employeeList.getSelectedValue().split(":");
            for (int i=0;i<restaurant.getUsers().getUsersArray().size();i++) {
                if (restaurant.getUsers().getUsersArray().get(i).getName().equals(nameEmp[1])) {
                    restaurant.getUsers().getUsersArray().get(i).setRole("Client");
                    try {
                        XMLInOut.Marshaller(restaurant);
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        managerDashboard.custListAncestorAdded(null);
        managerDashboard.employeeListAncestorAdded(null);
    }

    public void viewCustomers(){
        ArrayList<String> string = new ArrayList<String>();
        for(int i = 0; i<restaurant.getUsers().getUsersArray().size() ;i++){
            if (restaurant.getUsers().getUsersArray().get(i).getRole().equals("Client"))
                string.add(restaurant.getUsers().getUsersArray().get(i).getName());
        }
        String[] strings = managerDashboard.GetStringArray(string);
        managerDashboard.custList.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
    }

    public void viewTables(){
        DefaultTableModel tableModel = (DefaultTableModel) managerDashboard.tablesTable.getModel();
        int rowCount =tableModel.getRowCount();
        for (int i = 0; i <rowCount; i++) {
            tableModel.removeRow(0);
        }
        for(int i = 0 ; i < restaurant.getTables().getTables().size(); i++){
            tableModel.addRow(new Object[]{restaurant.getTables().getTables().get(i).getNumber(),restaurant.getTables().getTables().get(i).getNumberOfSeats(),restaurant.getTables().getTables().get(i).isSmoking()?"Somking":"Non-Smoking",restaurant.getTables().getTables().get(i).isBooked()?"Booked":"Not Booked"});
        }
    }

    public void viewOrders(){
        double totalMoneyMade = 0;
        DefaultTableModel orderTableModel = (DefaultTableModel) managerDashboard.orderTable.getModel();
        int rowCount =orderTableModel.getRowCount();
        for (int i = 0; i <rowCount; i++) {
            orderTableModel.removeRow(0);
        }
        for(int i = 0 ; i < reservations.getReservations().size() ; i++){
            for (int j = 0; j < reservations.getReservations().get(i).getResOrder().size() ;j++ )
                if(reservations.getReservations().get(i).getResOrder().get(j).getAmount()>0) {
                    orderTableModel.addRow(new Object[]{reservations.getReservations().get(i).getName(), reservations.getReservations().get(i).getResTable().getNumber(), reservations.getReservations().get(i).getResOrder().get(j).getName(), reservations.getReservations().get(i).getResOrder().get(j).getAmount()});
                    totalMoneyMade += reservations.getReservations().get(i).getResOrder().get(j).getPrice()*reservations.getReservations().get(i).getResOrder().get(j).getAmount();
                }
        }
        managerDashboard.totMoney.setText(totalMoneyMade + " L.E");
    }
}
