package com.company;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "dish")
@XmlAccessorType(XmlAccessType.FIELD)
public class Dish {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "price")
    private double price;
    @XmlElement(name = "type")
    private String type;
    public Dish(){}

    public Dish(String name, double price, String type) {
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getType() {return type; }
    public void setType(String type) {this.type = type; }
}
