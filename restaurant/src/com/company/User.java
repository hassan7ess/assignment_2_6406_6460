package com.company;

import javax.xml.bind.annotation.*;


@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User{

    private String name;
    private String username;
    private String password;
    private String role;

    public String getRole() { return role; }
    public void setRole(String role) {
        this.role = role;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
}
