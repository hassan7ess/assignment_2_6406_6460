This is assignmanet 2 for IDs 
	6406 Hassan Elsawy			  
	6460 Ahmad Wael

Index 
1.Brief description of application
2.Division of labour
3.an important warning

-----------------------------------------------------------------------------------------------------------------------------------------------------------------

the project aims to create a restaurant system where a customer could book a table and put an order for later

when the project is executed , 
A Welcome page is opened showing a list of the available Dishes served at the restaurant and a Login button

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
The login button opens the login page where the user either
-enters the correct credentials and logs in the the coresponding page (Cook , Waiter ,Manager or Customer)
-enters incorrect credentials where a window opens asking them for the correct credntials
or
-presses the register button where the account's information is to be entered and a new user of type customer is created  

after login or registration is done the corresponding page is opened
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
CustomerScreen contains

* 2 lists

	one that displays the dishes available where the dish is selected to be added to their order using the
	Add to Order button the other displays the chosen items , amounts and price  were they could be selected
	and removed using the Remove from Order button 

* cancel button
	
	cancels the order and closes the screen

* Order button
	
	finalizes the order and makes a reservation in the reservations.xml file

* A table button 
	
	which ,  on pressing ,opens a screen where the customer is to select one of the open tables

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
CookDashboard contains

* A table

	which shows the Dishes , Amount and Table of all reservations

*Pick up button

	which removes the current dish in order //note : all dishes of a  current reservation must be removed so
	that it could be delivered by the Waiter

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
WaiterDashboard contains

*A table 

	which the customer and table number of each reservation is displayed

*A Delivered button

	which removes the current reservation if the cook already cooked its order

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
ManagerDashboard contains 3 tabs

*tab 1 contains 

	the users of the system whether employees or customers 
	an Employ button which changes the role of a customer to the chosen job (Waiter , Cook or Manager)

*tab 2 contains

	a list of tables with their number of seats , type and status diplayed

*tab 3 contains

	the current orders and the total money earned from these orders

-----------------------------------------------------------------------------------------------------------------------------------------------------------------

Division of labour:

Hassan khaled 6406

	created xml fil handeling
	started Uml chart
	created jar file 
	started the customer and customer dashboard logic(Table selection logic and remove button)
	created Waiter dashboard display logic
	helped in resolving errors

Ahmad Wael 6460

	finished the remaining waiter dashboard logic
	created the Cook and Cook dashboard logic
	created the Manager and Manager dashboard logic
	completed the customer and customer dashboard logic(both lists , the add to order button , the order button and cancel button )
	improved UML chart by adding all setters and getters and connceting the classes correctly
	made GUI
	helped in resolving errors


-----------------------------------------------------------------------------------------------------------------------------------------------------------------

THE RESERVATION.XML AND RESTAURANT.XML FILES SHOULD BE PRESENT IN THE SAME FILE AS THE JAR EXECUTABLE AT LAUNCH !!!!!!!!!


Accounts
    
    username adam
    password adam_manager
    role Manager

    username john
    password john_doe
    role Customer

    username brian
    password mdir@admj%ar5qX2
    role Customer

    username george
    password k6987_#LpQ
    role Waiter

    username hobbens_tom
    password cooker_pass
    role Cooker

    username 1
    password 1
    role Customer




